###### `Note:` This plugin requires opposing collision objects to allow hits to work. (Hit Detection).

## Setup
1. Added files to Unreal Engine project inside of the content folder.
2. Open BS_Parent and under the functions section, find "Implementation Example".
3. Copy the code and move to your character's blueprint.
4. Connect the desired triggers (Ex. trigger on keypress) and re-add any nodes that give errors.
5. Profit!?

## Current Version
`Unreal Engine 4.19`